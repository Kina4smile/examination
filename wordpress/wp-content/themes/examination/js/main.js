$(document).ready(function() {

    var $menu = $("header");

    $(window).scroll(function () {
        if ($(this).scrollTop() > 610 ) {
            $menu.addClass("grey-bg");
        } else if ($(this).scrollTop() <= 610) {
            $menu.removeClass("grey-bg");
        }
    });//scroll
});