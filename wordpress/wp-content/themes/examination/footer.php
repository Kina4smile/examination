<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package examination
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container row">
			<div class="col-xs-6">
				<div class="col-xs-12 col-md-6">
					<?php
					if ( get_theme_mod('logo_img') ) : ?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_theme_mod('logo_img') ?>"
																												   alt=""></a></h1>
					<?php else : ?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<?php
					endif; ?>
				</div>
				<div class="col-xs-12 col-md-6">
					<?php dynamic_sidebar( 'footer-1' ); ?>
				</div>
			</div>
			<div class="col-xs-6">
				<?php dynamic_sidebar( 'footer-2' ); ?>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
