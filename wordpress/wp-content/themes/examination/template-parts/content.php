<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package examination
 */

?>
	<article class="row container" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="col-xs-1">
			<div class="author-avatar">
				<?php echo get_avatar( get_the_author_meta( 'ID' ), 60 ); ?>
			</div>
		</div>
		<div class="col-xs-11">
			<div class="entry-header">
				<?php
					if ( is_single() ) {
						the_title( '<h1 class="section-title">', '</h1>' );
					} else {
						the_title( '<h2><a class="section-title" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					}

				if ( 'post' === get_post_type() ) : ?>
				<div class="entry-meta">
					<p class="post-info">Posted by:<?php echo get_the_author(); ?>. <?php the_time('F-j-Y'); ?></p>
				</div><!-- .entry-meta -->
				<?php
				endif; ?>
			</div><!-- .entry-header -->

			<div class="entry-content">
				<?php the_post_thumbnail(); ?>
				<div class="main-text">
					<?php
					if ( is_single() ) {
						the_content();
					} else {
						echo get_the_excerpt();
					}
					?>
				</div>
			</div><!-- .entry-content -->

			<div class="entry-footer">
				<div class="social"></div>
				<?php if ( !is_single() ) { ?>
					<a href="<?php the_permalink(); ?>" class="main-btn">Reed more</a>
				<?php }?>
				
			</div><!-- .entry-footer -->
		</div>
	</article><!-- #post-## -->

