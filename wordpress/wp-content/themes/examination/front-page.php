<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package examination
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <div id="main" class="site-main" role="main">

            <!--TODO-->
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                <?php
                $args = array( 'post_type' => 'slides', 'posts_per_page' => 1 );
                $the_query = new WP_Query( $args );
                ?>
                <?php if ( $the_query->have_posts() ) : ?>
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="item active">
                            <?php the_post_thumbnail(); ?>
                            <div class="carousel-caption">
                                <?php the_title(); ?>
                                <?php the_content(); ?>
                            </div>
                        </div>
                    <?php endwhile;
                else:  ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>
                </div>
                
            </div>

            <!--About us-->
            <div class="about-us">
                <div class="container section row">
                    <div class="section-title col-xs-12 col-md-5">
                        <?php echo get_theme_mod('about_section_title');?>
                        <span>
                            <?php echo get_theme_mod('about_section_subtitle');?>
                        </span>
                    </div>
                    <div class="main-text col-xs-12 col-md-7 row">
                        <?php echo get_theme_mod('about_section_text');?>
                        <a href="<?php echo get_page_link(8); ?>" class="main-btn">Read More</a>
                    </div>
                </div>
            </div>


            <!--services-->
            <div class="services">
                <div class="container section row">
                    <div class="section-title"><?php echo get_theme_mod('service_section_title');?>
                        <span><?php echo get_theme_mod('service_section_subtitle');?></span>
                    </div>
                    <ul class="services-list">
                        <?php
                        $args = array( 'post_type' => 'services', 'posts_per_page' => 4 );
                        $the_query = new WP_Query( $args );
                        ?>
                        <?php if ( $the_query->have_posts() ) : ?>
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                <li class="col-md-6 col-sm-12">
                                    <div class="single-service row">
                                        <div class="single-service-img col-xs-2">
                                            <?php the_post_thumbnail(); ?>
                                        </div>
                                        <div class="single-service-content col-xs-10">
                                            <h4 class="section-heading"><?php the_title(); ?></h4>
                                            <p>
                                                <?php echo get_the_content(); ?>
                                            </p>
                                        </div>
                                        <?php wp_reset_postdata(); ?>
                                    </div>
                                </li>
                            <?php endwhile;
                        else:  ?>
                            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                        <?php endif; ?>
                    </ul>
                    <a href="#" class="main-btn">View more</a>
                </div>
            </div>

<!--            Comment section-->
<!--            TODO-->
            <div class="client-comments">
                <div class="container section">
                    <div class="section-title"><?php echo get_theme_mod('comment_section_title');?>
                        <span><?php echo get_theme_mod('comment_section_subtitle');?></span>
                    </div>
                    <div class="comments-list row">
                        <?php
                        $args = array();
                        $comments_query = new WP_Comment_Query;
                        $comments = $comments_query->query( $args );
                        if ( !empty( $comments ) ) {
                            foreach ( $comments as $comment ) { ?>
                                <div class="single-comment col-xs-12 col-md-4">
                                    <p class="main-text">
                                        <?php echo $comment->comment_content; ?>
                                    </p>
                                </div>
                           <?php }
                        } else {
                            echo 'No comments found.';
                        }
                        ?>
                    </div>
                </div>
            </div>
            
<!--            News-->
            <div class="news">
                <div class="container section">
                    <div class="section-title"><?php echo get_theme_mod('news_section_title');?>
                        <span><?php echo get_theme_mod('news_section_subtitle');?></span>
                    </div>
                    <div class="news-content row">
                        <div class="col-xs-12 col-md-6 row main-news">
                            <?php
                            $args = array( 'post_type' => 'post', 'posts_per_page' => 1 );
                            $the_query = new WP_Query( $args );
                            ?>
                            <?php if ( $the_query->have_posts() ) : ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                    <div class="col-xs3">
                                        <div class="post-info">
                                            <p class="date-day">
                                                <?php the_time('j'); ?>
                                            </p>
                                            <p>
                                                <?php the_time('F-Y'); ?>
                                            </p>
                                        </div>
                                        <div class="post-info main-text">
                                            <span class="comments-count"></span>
                                            <?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-9 post-content">
                                        <?php the_post_thumbnail(); ?>
                                        <h4 class="section-heading"><?php the_title(); ?></h4>
                                        <p class="main-text">
                                            <?php echo get_the_excerpt(); ?>
                                        </p>
                                    </div>
                                    <?php wp_reset_postdata(); ?>
                                <?php endwhile;
                            else:  ?>
                                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <?php
                            $args = array( 'post_type' => 'post', 'posts_per_page' => 2 );
                            $the_query = new WP_Query( $args );
                            ?>
                            <?php if ( $the_query->have_posts() ) : ?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                    <div class="single-new">
                                        <h4 class="section-heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                        <p class="accent-text"><?php the_time('j-F-Y'); ?></p>
                                        <p class="main-text">
                                            <?php echo get_the_excerpt(); ?>
                                        </p>
                                    </div>
                                    <?php wp_reset_postdata(); ?>
                                <?php endwhile;
                            else:  ?>
                                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                            <?php endif; ?>
                            <a href="<?php echo get_page_link(6); ?>" class="main-btn">View More</a>
                        </div>
                    </div>
                </div>
            </div>

<!--            Partners-->
            <div class="partners">
                <div class="container section">
                    <div class="section-title"><?php echo get_theme_mod('partners_section_title');?>
                        <span><?php echo get_theme_mod('partners_section_subtitle');?></span>
                    </div>
                    <ul class="row partners-list">
                        <?php
                        $args = array( 'post_type' => 'partners', 'posts_per_page' => 6 );
                        $the_query = new WP_Query( $args );
                        ?>
                        <?php if ( $the_query->have_posts() ) : ?>
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                <li class="col-xs-6 col-sm-4 col-md-2">
                                    <?php the_post_thumbnail(); ?>
                                </li>
                                <?php wp_reset_postdata(); ?>
                            <?php endwhile;
                        else:  ?>
                            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
