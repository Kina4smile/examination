<?php
/**
 * examination Theme Customizer.
 *
 * @package examination
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function examination_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	// Choose logo img
	$wp_customize->add_setting( 'logo_img' , array(
		'default'     => '',
		'transport'   => 'refresh',
	) );
	$wp_customize->add_section( 'examination_logo_image' , array(
		'title'      => __( 'Upload Logo', 'examination' ),
		'priority'   => 30,
	) );
	$wp_customize->add_control(new WP_Customize_Upload_Control($wp_customize, 'logo_img', array(
		'label'      => __( 'Logo Image', 'examination' ),
		'section'    => 'examination_logo_image',
		'settings'   => 'logo_img',
	) ) );
	// Choose headers img
	$wp_customize->add_setting( 'header_img' , array(
		'default'     => '',
		'transport'   => 'refresh',
	) );
	$wp_customize->add_section( 'examination_header_image' , array(
		'title'      => __( 'Upload header', 'examination' ),
		'priority'   => 30,
	) );
	$wp_customize->add_control(new WP_Customize_Upload_Control($wp_customize, 'header_img', array(
		'label'      => __( 'Header Image', 'examination' ),
		'section'    => 'examination_header_image',
		'settings'   => 'header_img',
	) ) );
	// Add About section
	$wp_customize->add_section('about_section', array(
		'title' => __('About section', 'examination'),
		'priority' => 30,
	));
	$wp_customize->add_setting('about_section_title', array(
		'default' => '',
		'transport' => 'refresh',
	));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'about_section_title', array(
		'label' => __('Section title', 'examination'),
		'section' => 'about_section',
		'settings' => 'about_section_title',
	)));
	$wp_customize->add_setting('about_section_subtitle', array(
		'default' => '',
		'transport' => 'refresh',
	));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'about_section_subtitle', array(
		'label' => __('Section subtitle', 'examination'),
		'section' => 'about_section',
		'settings' => 'about_section_subtitle',
	)));
	$wp_customize->add_setting('about_section_text', array(
		'default' => '',
		'transport' => 'refresh',
	));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'about_section_text', array(
		'label' => __('Section description', 'examination'),
		'section' => 'about_section',
		'settings' => 'about_section_text',
	)));
	// Add Services section
	$wp_customize->add_section('service_section', array(
		'title' => __('Our services section', 'examination'),
		'priority' => 30,
	));
	$wp_customize->add_setting('service_section_title', array(
		'default' => '',
		'transport' => 'refresh',
	));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'service_section_title', array(
		'label' => __('Section title', 'examination'),
		'section' => 'service_section',
		'settings' => 'service_section_title',
	)));
	$wp_customize->add_setting('service_section_subtitle', array(
		'default' => '',
		'transport' => 'refresh',
	));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'service_section_subtitle', array(
		'label' => __('Section subtitle', 'examination'),
		'section' => 'service_section',
		'settings' => 'service_section_subtitle',
	)));
	// Add Comment section
	$wp_customize->add_section('comment_section', array(
		'title' => __('Comment section', 'examination'),
		'priority' => 30,
	));
	$wp_customize->add_setting('comment_section_title', array(
		'default' => '',
		'transport' => 'refresh',
	));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'comment_section_title', array(
		'label' => __('Section title', 'examination'),
		'section' => 'comment_section',
		'settings' => 'comment_section_title',
	)));
	$wp_customize->add_setting('comment_section_subtitle', array(
		'default' => '',
		'transport' => 'refresh',
	));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'comment_section_subtitle', array(
		'label' => __('Section subtitle', 'examination'),
		'section' => 'comment_section',
		'settings' => 'comment_section_subtitle',
	)));
	// Add News section
	$wp_customize->add_section('news_section', array(
		'title' => __('News section', 'examination'),
		'priority' => 30,
	));
	$wp_customize->add_setting('news_section_title', array(
		'default' => '',
		'transport' => 'refresh',
	));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'news_section_title', array(
		'label' => __('Section title', 'examination'),
		'section' => 'news_section',
		'settings' => 'news_section_title',
	)));
	$wp_customize->add_setting('news_section_subtitle', array(
		'default' => '',
		'transport' => 'refresh',
	));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'news_section_subtitle', array(
		'label' => __('Section subtitle', 'examination'),
		'section' => 'news_section',
		'settings' => 'news_section_subtitle',
	)));
	// Add Partners section
	$wp_customize->add_section('partners_section', array(
		'title' => __('Partners section', 'examination'),
		'priority' => 30,
	));
	$wp_customize->add_setting('partners_section_title', array(
		'default' => '',
		'transport' => 'refresh',
	));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'partners_section_title', array(
		'label' => __('Section title', 'examination'),
		'section' => 'partners_section',
		'settings' => 'partners_section_title',
	)));
	$wp_customize->add_setting('partners_section_subtitle', array(
		'default' => '',
		'transport' => 'refresh',
	));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'partners_section_subtitle', array(
		'label' => __('Section subtitle', 'examination'),
		'section' => 'partners_section',
		'settings' => 'partners_section_subtitle',
	)));
	// Add Contact us section
	$wp_customize->add_section('contact_section', array(
		'title' => __('Contact us section', 'examination'),
		'priority' => 30,
	));
	$wp_customize->add_setting('contact_phone', array(
		'default' => '',
		'transport' => 'refresh',
	));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'contact_phone', array(
		'label' => __('Your contact phone', 'examination'),
		'section' => 'contact_section',
		'settings' => 'contact_phone',
	)));
	$wp_customize->add_setting('contact_email', array(
		'default' => '',
		'transport' => 'refresh',
	));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'contact_email', array(
		'label' => __('Section subtitle', 'examination'),
		'section' => 'contact_section',
		'settings' => 'contact_email',
	)));
}
add_action( 'customize_register', 'examination_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function examination_customize_preview_js() {
	wp_enqueue_script( 'examination_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'examination_customize_preview_js' );
